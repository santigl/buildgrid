# Debian Stretch base image with python 3.5:
#
image: python:3.5-stretch

stages:
  - test
  - post
  - deploy


# Templates and global variables.
#
variables:
  BGD: "${CI_PROJECT_DIR}/env/bin/bgd"
  PYTHON: "${CI_PROJECT_DIR}/env/bin/python"
  TOOLS: "${CI_PROJECT_DIR}/env/bin"

.build-template:
  before_script: &install-base
    - apt-get update && apt-get install -y python3 python3-venv python3-pip graphviz
    - python3 -m venv env  # Creates a virtual environment
    - ${PYTHON} -m pip --cache-dir=.pip install --upgrade setuptools pip wheel
    - ${PYTHON} -m pip --cache-dir=.pip install --editable ".[auth,docs,tests]"
  cache:
    paths:
      - .pip


# Test stage, build and test the code.
#
unit-tests:
  stage: test
  extends: .build-template
  script:
    - ${PYTHON} setup.py test
  after_script:
    - mkdir -p coverage/
    - cp .coverage coverage/coverage."${CI_JOB_NAME}"
  variables:
    PYTEST_ADDOPTS: "--color=yes"
  artifacts:
    paths:
      - coverage/

dummy-tests:
  stage: test
  extends: .build-template
  script:
    - ${BGD} server start data/config/default.conf &
    - sleep 1  # Allows server to boot
    - ${BGD} bot dummy &
    - ${BGD} cas upload-dummy
    - ${BGD} execute request-dummy --wait-for-completion

e2e-tests:
  stage: test
  image: registry.gitlab.com/buildgrid/buildbox/buildbox-e2e:latest
  script:
    - BUILDGRID_SOURCE_ROOT=`pwd` end-to-end-test.sh


# Post-build stage, documentation, coverage report...
#
documentation:
  stage: post
  extends: .build-template
  script:
    - PATH="${PATH}:${TOOLS}" make -C docs html
  after_script:
    - mkdir -p documentation/
    - cp -a docs/build/html/. documentation/
  artifacts:
    paths:
      - documentation/

coverage:
  stage: post
  extends: .build-template
  dependencies:
    - unit-tests
  coverage: '/TOTAL +\d+ +\d+ +(\d+\.\d+)%/'
  script:
    - cd coverage/ && ls -l .
    - ${PYTHON} -m coverage combine --rcfile=../.coveragerc --append coverage.*
    - ${PYTHON} -m coverage html --rcfile=../.coveragerc --directory .
    - ${PYTHON} -m coverage report --rcfile=../.coveragerc --show-missing
  artifacts:
    paths:
      - coverage/

# Deployement stage, only for merges which land on master branch.
#
pages:
  stage: deploy
  dependencies:
    - coverage
    - documentation
  script:
    - mkdir -p public/coverage/
    - cp -a coverage/* public/coverage/
    - ls -la public/coverage/
    - cp -a documentation/* public/
    - ls -la public/
  artifacts:
    paths:
      - public/
  only:
    - master

triggers:
  stage: deploy
  before_script:
    - apt-get update && apt-get install -y curl
  script:
    - curl --request POST --form "token=$CI_JOB_TOKEN" --form ref=master https://gitlab.com/api/v4/projects/buildgrid%2Fbuildbox%2Fbuildbox-e2e/trigger/pipeline
  variables:
    GIT_STRATEGY: none
  only:
    - master
